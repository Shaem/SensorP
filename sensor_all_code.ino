int temp = A1;
int buzzer = 11;
int LED = 8;
int trig = A5;
int echo = A4;
void setup() {
  pinMode (temp, INPUT);
  pinMode(LED, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(5, HIGH);
  pinMode(LED, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  Serial .begin(74880);

}
float distance() {
  float duration = 0.00;
  float cm = 0.00;
  digitalWrite(trig, LOW);
  delayMicroseconds(5);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(echo, LOW);
  duration = pulseIn(echo, HIGH);
  cm = (duration / 58.82);
  return cm;
}
void loop() {
  float value  = analogRead (temp);
  float mv = (5 * 1000 * value) / 1024;
  float degree = mv / 10;
  if (degree > 25) {
    digitalWrite(LED, HIGH);
    delay(1000);
    digitalWrite(LED, LOW);
    delay(200);

  }
  float distance2 = distance();
  if (distance2 > 50) {
    digitalWrite(buzzer, HIGH);
    delay(500);
    digitalWrite(buzzer, LOW);

    delay(200);
  }
  Serial.print ("Temperature:");
  Serial.println(degree);
  Serial.print("distance:");
  Serial.println (distance2);
  delay(300);
}
